#define PURPLE_PLUGINS

#include <glib.h>

#include "notify.h"
#include "plugin.h"
#include "version.h"

static gboolean
plugin_load(PurplePlugin *plugin) {
    purple_notify_message(plugin, PURPLE_NOTIFY_MSG_INFO, "Hello World!",
                        "This is the Hello World! plugin :)", NULL, NULL, NULL);

    return TRUE;
}

static PurplePluginInfo info = {
    PURPLE_PLUGIN_MAGIC,
    PURPLE_MAJOR_VERSION,
    PURPLE_MINOR_VERSION,
    PURPLE_PLUGIN_STANDARD,
    NULL,
    0,
    NULL,
    PURPLE_PRIORITY_DEFAULT,

    "signal-libpurple",
    "Signal link",
    "0.1",

    "Link to a mobile device running signal, to send and receive messages.",
    "Link to a mobile device running signal, to send and receive messages. This operates much like the Signal desktop application, but aims to be more lightweight by integrating itself as a libpurple plugin (which can be used by the Pidgin IM client).",
    "Thomas Woltjer <thomas@thomaswoltjer.com>",
    "https://gitlab.com/twoltjer/signal-libpurple",

    plugin_load,                   
    NULL,
    NULL,

    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL
};

static void
init_plugin(PurplePlugin *plugin)
{
}

PURPLE_INIT_PLUGIN(hello_world, init_plugin, info)
